package com.adarsh.ipl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static final int MATCH_ID = 0;
    public static final int MATCH_SEASON = 1;
    public static final int MATCH_WINNER = 10;

    public static final int DELIVERY_MATCH_ID = 0;
    public static final int DELIVERY_INNING = 1;
    public static final int DELIVERY_BOWLING_TEAM = 3;
    public static final int DELIVERY_OVER = 4;
    public static final int DELIVERY_BALL = 5;
    public static final int DELIVERY_BOWLER = 8;
    public static final int DELIVERY_EXTRA_RUNS = 16;
    public static final int DELIVERY_TOTAL_RUNS = 17;

    public static void main(String[] args) {
        List<Match> matches = getMatches();
        List<Delivery> deliveries = getDeliveriesData();

        findNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWonPerTeam(matches);
        findExtraRunsConcededPerTeamIn2016(matches, deliveries);
        findMostEconomicalBowlerIn2015(matches, deliveries);
        findHighestScoreOfAllTimesInOneInnings(deliveries);
    }

    private static void findHighestScoreOfAllTimesInOneInnings(List<Delivery> deliveries) {
        Map<Integer, Integer> maxRunPerMatch = new HashMap<>();
        int maxScore = 0;

        for (int ball = 0; ball < deliveries.size(); ball++) {
            int currentMatchId = deliveries.get(ball).getMatchId();
            int firstInningScore = 0;
            int secondInningScore = 0;
            while (ball < deliveries.size() && currentMatchId == deliveries.get(ball).getMatchId()) {
                if (deliveries.get(ball).getInning() == 1) {
                    firstInningScore += deliveries.get(ball).getTotalRuns();
                } else if (deliveries.get(ball).getInning() == 2) {
                    secondInningScore += deliveries.get(ball).getTotalRuns();
                }
                ball++;
            }
            ball--;
            maxRunPerMatch.put(currentMatchId, Math.max(firstInningScore, secondInningScore));
        }
        for (Map.Entry<Integer, Integer> entry : maxRunPerMatch.entrySet()) {
            if (maxScore < entry.getValue()) {
                maxScore = entry.getValue();
            }
        }
        System.out.println("Maximum Score of all times");
        System.out.println(maxScore);
        // TODO  better name scoreOfInning , better data structure
    }

    private static void findMostEconomicalBowlerIn2015(List<Match> matches, List<Delivery> deliveries) {
        int year = 2015;
        Set<Integer> matchIds2015 = new HashSet<>();//TODO set instead of arraylist
        Map<String, Map<Integer, Integer>> totalRunsByBowlerEachOver = new HashMap<>();
        Map<String, Double> economyOfBowlers = new HashMap<>();

        for (Match match : matches) {
            if (match.getSeason() == year) {
                matchIds2015.add(match.getId());
            }
        }
        for (int ball = 0; ball < deliveries.size(); ball++) {
            if (matchIds2015.contains(deliveries.get(ball).getMatchId())) {
                int firstBall = ball;
                String bowlerName = deliveries.get(ball).getBowler();
                int runs = 0;
                while (deliveries.get(firstBall).getOver() == deliveries.get(ball).getOver()) {
                    runs += deliveries.get(ball).getTotalRuns();
                    ball++;
                }
                if (!totalRunsByBowlerEachOver.containsKey(bowlerName)) {
                    Map<Integer, Integer> runInOver = new HashMap<>();
                    runInOver.put(1, runs);
                    totalRunsByBowlerEachOver.put(bowlerName, runInOver);
                } else {
                    for (Map.Entry<Integer, Integer> entry : totalRunsByBowlerEachOver.get(bowlerName).entrySet()) {
                        Map<Integer, Integer> runInOver = new HashMap<>();
                        runInOver.put(entry.getKey() + 1, entry.getValue() + runs);
                        totalRunsByBowlerEachOver.put(bowlerName, runInOver);
                    }
                }
            }
        }
        for (Map.Entry<String, Map<Integer, Integer>> entry : totalRunsByBowlerEachOver.entrySet()) {
            for (Map.Entry<Integer, Integer> entry1 : entry.getValue().entrySet()) {
                economyOfBowlers.put(entry.getKey(), (double) entry1.getValue() / entry1.getKey());
            }
        }
//TODO make more readable
//TODO sorted data structure
        System.out.println("Most Economical Bowlers");
        List<Map.Entry<String, Double>> sortedBowlerEconomy = new ArrayList<>(economyOfBowlers.entrySet());
        sortedBowlerEconomy.sort(Map.Entry.comparingByValue());
        for (Map.Entry<String, Double> map : sortedBowlerEconomy) {
            System.out.println(map.getKey() + " ------ " + map.getValue());
        }
        System.out.println();
    }

    private static void findExtraRunsConcededPerTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        int year = 2016;
        Set<Integer> matchIds2016 = new TreeSet<>(); //TODO list-->set
        Map<String, Integer> extraRunsConcededPerTeamIn2016 = new HashMap<>();

        for (Match match : matches) {
            if (match.getSeason() == year) {
                matchIds2016.add(match.getId());
            }
        }
        for (Delivery delivery : deliveries) {
            if (matchIds2016.contains(delivery.getMatchId())) {
                if (!extraRunsConcededPerTeamIn2016.containsKey(delivery.getBowlingTeam())) {
                    extraRunsConcededPerTeamIn2016.put(delivery.getBowlingTeam(), delivery.getExtraRuns());
                } else {
                    extraRunsConcededPerTeamIn2016.put(delivery.getBowlingTeam(),
                            extraRunsConcededPerTeamIn2016.get(delivery.getBowlingTeam()) + delivery.getExtraRuns());
                }
            }
        }
        System.out.println("For the year 2016 get the extra runs conceded per team");
        for (Map.Entry<String, Integer> entry : extraRunsConcededPerTeamIn2016.entrySet()) {
            System.out.println(entry.getKey() + " ------ " + entry.getValue());
        }
        System.out.println();
    }

    private static void findNumberOfMatchesWonPerTeam(List<Match> matches) {
        Map<String, Integer> numberOfMatchesWonPerTeam = new HashMap<>();//TODO misinterpreted

        for (Match match : matches) {
            if (!numberOfMatchesWonPerTeam.containsKey(match.getWinner())) {
                numberOfMatchesWonPerTeam.put(match.getWinner(), 1);
            } else {
                numberOfMatchesWonPerTeam.put(match.getWinner(), numberOfMatchesWonPerTeam.get(match.getWinner()) + 1);
            }
        }
        System.out.println("Number of matches won of all teams over all the years of IPL");
        numberOfMatchesWonPerTeam.remove("");
        for (Map.Entry<String, Integer> entry : numberOfMatchesWonPerTeam.entrySet()) {
            System.out.println(entry.getKey() + " ------ " + entry.getValue());
        }
        System.out.println();
    }

    private static void findNumberOfMatchesPlayedPerYear(List<Match> matches) {
        Map<Integer, Integer> numberOfMatchesPlayedPerYear = new HashMap<>();

        for (Match match : matches) {
            if (!numberOfMatchesPlayedPerYear.containsKey(match.getSeason())) {
                numberOfMatchesPlayedPerYear.put(match.getSeason(), 1);
            } else {
                numberOfMatchesPlayedPerYear.put(match.getSeason(), numberOfMatchesPlayedPerYear.get(match.getSeason()) + 1);
            }
        }
        System.out.println("Number of matches played per year of all the years in IPL");
        for (Map.Entry<Integer, Integer> entry : numberOfMatchesPlayedPerYear.entrySet()) {
            System.out.println(entry.getKey() + " ------ " + entry.getValue());
        }
        System.out.println();
    }

    private static List<Delivery> getDeliveriesData() {
        List<Delivery> deliveries = new ArrayList<>();
        String csvFile = "src/deliveries.csv";
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            String line = "";
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] rowSplit = line.split(",");
                Delivery delivery = new Delivery();
                delivery.setMatchId(Integer.parseInt(rowSplit[DELIVERY_MATCH_ID]));
                delivery.setInning(Integer.parseInt(rowSplit[DELIVERY_INNING]));
                delivery.setBowlingTeam(rowSplit[DELIVERY_BOWLING_TEAM]);
                delivery.setOver(Integer.parseInt(rowSplit[DELIVERY_OVER]));
                delivery.setBall(Integer.parseInt(rowSplit[DELIVERY_BALL]));
                delivery.setBowler(rowSplit[DELIVERY_BOWLER]);
                delivery.setExtraRuns(Integer.parseInt(rowSplit[DELIVERY_EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(rowSplit[DELIVERY_TOTAL_RUNS]));
                deliveries.add(delivery);
            }//TODO throw an error in case there is no element in the cell i.e. null pointer exception
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return deliveries;
    }

    private static List<Match> getMatches() {
        List<Match> matches = new ArrayList<>();
        String csvFile = "src/matches.csv";//TODO no absolute path
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            String line = "";
            br.readLine();
            while ((line = br.readLine()) != null) {
                Match match = new Match();
                String[] matchRow = line.split(",");
                match.setId(Integer.parseInt(matchRow[MATCH_ID]));
                match.setSeason(Integer.parseInt(matchRow[MATCH_SEASON]));
                match.setWinner(matchRow[MATCH_WINNER]);
                matches.add(match);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return matches;
    }
}
