package com.adarsh.ipl;

public class Delivery {
    private int matchId;
    private int inning;
    private String bowlingTeam;
    private int over;
    private int ball;
    private String Bowler;
    private int extraRuns;
    private int totalRuns;

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getInning() {
        return inning;
    }

    public void setInning(int inning) {
        this.inning = inning;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(String bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public int getOver() {
        return over;
    }

    public void setOver(int over) {
        this.over = over;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    public String getBowler() {
        return Bowler;
    }

    public void setBowler(String bowler) {
        Bowler = bowler;
    }

    public int getExtraRuns() {
        return extraRuns;
    }

    public void setExtraRuns(int extraRuns) {
        this.extraRuns = extraRuns;
    }

    public int getTotalRuns() {
        return totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    @Override
    public String toString() {
        return "Delivery{" +
                "matchId=" + matchId +
                ", inning=" + inning +
                ", bowlingTeam='" + bowlingTeam + '\'' +
                ", over=" + over +
                ", ball=" + ball +
                ", Bowler='" + Bowler + '\'' +
                ", extraRuns=" + extraRuns +
                ", totalRuns=" + totalRuns +
                '}';
    }
}
